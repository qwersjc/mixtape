%% findAudioBreaks.m
% The goal of this .m file is to explore ways to find breaks in an audio
%  file, using time-domain analysis to find pauses in speech or musical
%  pieces. Once the breaks are found, those portions can be separated for
%  further processing.
% One goal is to turn this into a function.
clear, clc, close all;

% Loading in audio pieces (note: they must be in this directory)
bajwa = audioread('Bajwa40s.mp3');
guitar = audioread('Guitar_Melody.wav');
percus = audioread('Percussion.wav');

% This simple function normalizes the audio to have a max amplitude of 1.
% Its effectiveness is reduced if there is a single large spike in volume.
normalize = @(x) x/max(x);

% Preprocessing of audio files, clipping, dividing into channels, etc.
%b = bajwa(1:100000,:);
%b = bajwa(1:500000,:);
b = guitar(88000:end,:);
%b = percus;
bl = normalize(b(:,1)); %left channel
br = normalize(b(:,2)); %right channel
b_aud = audioplayer([bl,br], 44100); %allows for playing the initial audio
%% Windowing

i = 1;
M = 0;
I = 0;
windowsize = 420;
threshold = .075; %.1 = 10% cutoff
%bl = abs(bl);
envelope = zeros(length(bl),1);

%enveloping
while i < length(bl)
    if mod(i,windowsize) == 0
        a = bl((i-windowsize+1):i);
        [M,I] = max(a);
        if i>windowsize
            envelope(i-windowsize) = M;
        end
    else
        if i>windowsize
            envelope(i-windowsize) = M;
        end
    end
    i = i+1;
end

%LPF on envelope
% Ws = 4000;
% b = (1/Ws)*ones(1,Ws);
% envelope = filter(b, 1, envelope); 

envelope = envelope.*(envelope>threshold); %first threshold run

% threshold=.4;
% start = start.*(start>threshold); %second threshold run

speaking = double(envelope>threshold); %speaking is purple boxes
len = 1:length(bl);
figure;
hold on;
plot(len, bl);
plot(len, envelope.*(envelope>threshold));
plot(len, linspace(threshold,threshold,length(bl)), '--');
plot(len, speaking);
hold off;


ba = bl .* (envelope ~= 0); %makes zeros wherever not interested
bb = br .* (envelope ~= 0); %same
%ba = bl;
s_aud = audioplayer([ba,bb],44100);
%play(s_aud); %play the result
%% Find the location of the speaking

% v = speaking'; %// data
% w = [false v~=0 false]; %// "close" v with zeros, and transform to logical
% starts = find(w(2:end) & ~w(1:end-1)); %// find starts of runs of non-zeros
% ends = find(~w(2:end) & w(1:end-1))-1; %// find ends of runs of non-zeros
% speakingvector = arrayfun(@(s,e) v(s:e), starts, ends, 'uniformout', false); %// build result

t=0;
points=1;
for i=1:length(speaking)
    if t==0 && speaking(i) ==1 
        start=i; t=1;
    elseif (t==1) && (speaking(i)==0) 
        final=i; t=0;
        if final-start>10000 %at least 10000/44100 seconds
            arr{points}=[start,final];
             points=points+1;
        end
    end
end
%% Split up voice using speaking
for i=1:length(arr)
    leftspeechvector{i}=bl(arr{i}(1):arr{i}(2));
end
for i=1:length(arr)
    rightspeechvector{i}=br(arr{i}(1):arr{i}(2));
end
for i=1:length(leftspeechvector)
    fullspeechvector{i}=audioplayer([leftspeechvector{i},rightspeechvector{i}], 44100);
end
%% Frequency Analysis
for i=1:length(leftspeechvector)
    freq=abs(fft(leftspeechvector{i}))/length(leftspeechvector{i});
    leftfreqs{i}=freq(1:length(leftspeechvector{i})/2+1);
    leftfreqs{i}(2:end-1)=2*leftfreqs{i}(2:end-1);
end
index=4;
f=44100*(0:(length(leftspeechvector{index})/2))/length(leftspeechvector{index});
figure;
plot(f,leftfreqs{index})

%%

for i = 1:length(fullspeechvector)%:-1:1
    play(fullspeechvector{i});
    pause(length(leftspeechvector{i})/44100);
end
