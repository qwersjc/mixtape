% This function uses the inputted quantization interval (tolerance) to
% split the given vector into groups
function [groupInd]=Quantize(vec,tolerance)
    targetFreq = vec(1);
    start = 1;
    j = 1;
    groupInd = [];
    for i = 1:length(vec)
            if (vec(i)-targetFreq)>tolerance % new group
                groupInd{j} = start:(i-1);
                j = j+1;
                targetFreq = vec(i);
                start = i;
            end
            if i == length(vec) % edge case of the last group
                groupInd{j} = start:(i);
            end
    end
end