% This function determines the quantization interval that will result in
% the inputted number of groups
function [quantInterval]=findQuant(vec,numInt)
    quantInterval = 1; % initial guess
    while (quantInterval>0 & quantInterval<max(vec))    
        ind = Quantize(vec,quantInterval)
        quantInterval
        if length(ind)== numInt % when find the interval  
            break;
        elseif length(ind)>numInt
            quantInterval = quantInterval+quantInterval/2; %interval too small, increment
        else
            quantInterval = quantInterval-quantInterval/2; % interval too large, decrement
        end
    end
end