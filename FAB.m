function [cellout, varargout] = FAB(signal, varargin)

%FAB - Find Audio Breaks
% This function takes multiple inputs/outputs and separates an audio file
%  into segments where audio can be detected

% input args: (sig, thres, res, filt, plot)
%	signal (single channel)
%o	threshold (0,1] - threshold in percentage (signal is always normalized)
%o	resolution > 0 - resolution of envelope
%o  Mov. Avg. Filter window > 0 - sample window
%o  plot - [0/1]?
%o  Minimum time in window-sizes for sound to be included (default is 0)

% output args: (cellout, envelope, speaking, new_audio)
%   cell output - cell array of audio segments
%oo seg_list - position of cuts in original array
%oo	envelope - envelope signal
%oo speaking - speaking boolean signal
%oo	new audio file - new audio with zeroed out portions

    %% initial checking of input/output arguments
	if nargin < 1
		disp('Error: too few inputs\n');
		return;
    end
	nargoutchk(1,5); %checking output arguments
    
	testinput = size(signal);
	if testinput(1)~=1 && testinput(2)~=1
		disp('Error: signal must be 1-dimensional vector');
		return;
	else
		len = length(signal);
    end
    if testinput(1) == 1
        signal = signal';
    end
    
    %% initializing variables for function
	normalize = @(x) x/max(x);
	signal = normalize(signal);
    
    %s=0;
    %for i=1:length(signal)
    %    s = s+abs(signal(i));
    %end
    
	samplerate = 44100;
	resolution = round(samplerate*(.9/100)); %arbitrary window size, little less than 1/100th of a second
	threshold = .1;
    useLPF = 0;
    showPlot = 0;
    minlength = 0; %minimum sample length a peak needs to be to be included in final vector

	if nargin == 2 && isnumeric(varargin{1})
		threshold = varargin{1};
    elseif nargin == 3
        if isnumeric(varargin{1})
            threshold = varargin{1};
        end
        if isnumeric(varargin{2}) && varargin{2}>0
            resolution = round(varargin{2});
        end
    elseif nargin == 4
        if isnumeric(varargin{1})
            threshold = varargin{1};
        end
        if isnumeric(varargin{2}) && varargin{2}>0
            resolution = round(varargin{2});
        end
        if isnumeric(varargin{3}) && varargin{3}>0
            useLPF = varargin{3};
        end
    elseif nargin == 5
        if isnumeric(varargin{1})
            threshold = varargin{1};
        end
        if isnumeric(varargin{2}) && varargin{2}>0
            resolution = round(varargin{2});
        end
        if isnumeric(varargin{3}) && varargin{3}>0
            useLPF = varargin{3};
        end
        if isnumeric(varargin{4}) && varargin{4}>0
            showPlot = 1;
        end
    elseif nargin == 6
        if isnumeric(varargin{1})
            threshold = varargin{1};
        end
        if isnumeric(varargin{2}) && varargin{2}>0
            resolution = round(varargin{2});
        end
        if isnumeric(varargin{3}) && varargin{3}>0
            useLPF = varargin{3};
        end
        if isnumeric(varargin{4}) && varargin{4}>0
            showPlot = 1;
        end
        if isnumeric(varargin{5}) && varargin{5}>0
            minlength=varargin{5}; % number of windows
        end
    end 

	envelope = zeros(len, 1);

    %% start making maximal envelope of signal
	M = 0;
    if len > resolution
        for n = 1:len
            if mod(n,resolution) == 0
                test = signal((n-resolution+1):n);
                [M,~] = max(test);
                if n > resolution
                    envelope(n-resolution) = M;
                end
            elseif n > resolution
                envelope(n-resolution) = M;
            end
            %n = n + 1;
        end
    else
        
    end
	env_orig = envelope;
    
    %Moving Avg on envelope
    if useLPF > 0 && len > useLPF
        b = (1/useLPF)*ones(1,useLPF);
        envelope = filter(b, 1, envelope);
        % Compensating for the low pass phase shift
        % Filter has trouble detecting quick magnitude changes
        for n = 1:(len-round(useLPF/2))
            envelope(n) = envelope(n+round(useLPF/2));
        end
    end
    
    
	speaking = (envelope>threshold);
	%envelope = envelope .* speaking;
	sig_new  = signal .* speaking;

    varargout{2} = envelope;
    varargout{3} = speaking;
    varargout{4} = sig_new;

    %% start breaking audio into segments
	test = 0;
	segments = 1;
    seg_list{1} = 'No Segments';
	for n = 1:len
		if test==0 && speaking(n)==1
			start = n; test = 1;
		elseif test==1 && speaking(n)==0
			final = n; test = 0;
			if (final-start) > minlength*samplerate
				seg_list{segments} = [start, final];
				segments = segments + 1;
			end
		end
    end
    
    varargout{1} = seg_list;

    %% list of segments
    cellout = cell(length(seg_list),1);
	for n = 1:length(seg_list)
		cellout{n} = signal( seg_list{n}(1) : seg_list{n}(2) );
    end

    %% plotting if desired
    
    
    
    if showPlot > 0
        %making a signal of segments that made the cut
        final_speaking = linspace(0,0, len);
        for n = 1:length(seg_list)
            final_speaking(seg_list{n}(1):seg_list{n}(2)) = .5;
        end
        
        figure();
        hold on;
        lin = linspace(1,len,len);
        thres = linspace(threshold,threshold,len);
        plot(lin, signal, 'color', [0 .447 .741]);
        plot(lin, envelope, 'color', [.85 .325 .098]);
        plot(lin, thres, '--', 'color', [.929 .694 .125]);
        plot(lin, speaking, 'color', [.494 .184 .556]);
        plot(lin, final_speaking, 'color', [.466 .674 .184], 'linewidth', 2);
        hold off;
        legend('Audio', 'Envelope', 'Threshold', 'Detected Audio',...
               'Significant Audio', 'Location', 'Southeast');
        axis([1 len, -1.1 1.1]);
        xlabel('samples');
        ylabel('normalized amplitude');
        
    end


end

