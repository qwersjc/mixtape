%% Main Project File
clear, clc, close all;

% Loading in audio pieces for testing (note: they must be in this directory)
voice = audioread('AudioSamples/Prof40s.mp3');
music = audioread('AudioSamples/Guitar_Melody.wav');
% music = audioread('Sweet_Child_Of_Mine_Sample.wav');

% Chopping audio signals into sound segments
voice = .5*(voice(:,1) + voice(:,2)); % stereo -> mono channel
music = .5*(music(:,1) + music(:,2)); % stereo -> mono channel

voice = voice(01:500000); % trimming to a managable audio sample
music = music(88000:end); % removing pause in the beginning

NSB_s = 0.075; NSB_m = 0.1; % sound threshold
% for more info on finding audio breaks (FAB), please see the function.
[voice_segments,        ~        ] = FAB(voice, NSB_s, 420, 3000, 1, .2);
title('Audio Segmentation into Words');
[music_segments, segment_position] = FAB(music, NSB_m, 200, 0000, 1, .1);
title('Instrumental Music Segmentation into Notes');



% Putting it in a playable audio form-i.e. play(voicechopped{1})
% not used at the moment
% for i=1:length(voice_segments)
% 	voice_segments_audio{i} = audioplayer(voice_segments{i}, 44100);
% end
% for i=1:length(music_segments)
% 	music_segments_audio{i} = audioplayer(music_segments{i}, 44100);
% end


%% Taking the DFT (and plotting it)

% Taking speech DFT, plotting a sample
[f_voice, voice_freqs] = AnalyzeFreq(voice_segments);
index = 6;
figure;
%the plot shown shows which frequencies have high amplitudes for each sound
%segment.  The plot of the sixth voice segment is given as an example.
plot(f_voice{index}, voice_freqs{index});
title('Frequency of Spoken Words');
xlabel('Frequency (Hz)'); ylabel('Amplitude');

% Taking music DFT, plotting a sample
[f_music, music_freqs] = AnalyzeFreq(music_segments);
index = 2;
figure;
%This shows the frequencies prevalent in the second music segment sample.  
plot(f_music{index}, music_freqs{index});
title('Frequency of Music Notes');
xlabel('Frequency (Hz)'); ylabel('Amplitude');


%% Finding the average frequency of each sample for matching

for i=1:length(f_voice)
    averagevoice(i) = WA(f_voice{i},voice_freqs{i}); 
    %The WA function takes the weighted average of the prevalent
    %frequencies, allowing us to have a frequency associated with
    %each segment
end

figure;
plot(averagevoice);
title('Average Frequencies of Spoken Words');
xlabel('Word'); ylabel('Average Frequency (kHz)');

for i=1:length(f_music)
    averagemusic(i) = WA(f_music{i},music_freqs{i});
    %averagemusic gives us a list of frequencies with which we can
    %associate each segment of music
end
figure;
plot(averagemusic);
title('Average Frequencies of Music Sounds');
xlabel('Sound'); ylabel('Average Frequency (kHz)');

numNotes  = length(averagemusic);


%% Mapping Frequencies (Autotuning Included)

stretchedvoice = max(averagemusic) / max(averagevoice) * averagevoice;
% Determining what audio segments to map to the music segments
for i=1:length(averagemusic)
    for j=1:length(averagevoice)
        d(j)=abs(averagemusic(i)-averagevoice(j));
    end
    [~, allocatednotes(i)]=min(d);    
end

% Stringing audio segments together according to mapping results
music_copy = music;
for i=1:length(allocatednotes) - 3
    start=segment_position{i}(1); % 1 because it is starting index

    m = voice_segments{allocatednotes(i)};
    n = numel(m);
    k1(i) = averagevoice(allocatednotes(i))/averagemusic(i);
    xq = linspace(1, n, k1(i)*n);
    m2 = interp1(1:n, m, xq, 'nearest');
    %the interp1 function is used to stretch the voice vector (i.e. do autotuning), effectively
    %changing its pitch when played back at 44100Hz

    finish = segment_position{i}(1) + length(m2) - 1;
	if finish > length(music)
		finish = length(music);
		m2 = m2(1:finish-start+1);  
        %truncates the array m2 so that its size does not exceed the size
        %of the music segment it corresponds to
	end

    music_copy(start:finish) = .8 * music(start:finish) + m2';
    %music_copy will contain an audio matrix consisting of the original BGM
    %with the pseudo-autotuned voice segments added at the proper
    %locations.
end


COMBINED_AUDIO = audioplayer(music_copy, 44100);
%make an audioplayer object of the combined music
play(COMBINED_AUDIO);

cjc = averagevoice(allocatednotes);

% Plot Mapping Results
figure;
hold on;
plot(averagemusic,'r','Linewidth',2);
plot(averagevoice(allocatednotes),'b','Linewidth',2);
plot(cjc(1:end-3).*(1./k1),'g--','Linewidth',2);
hold off;
legend('Music Notes','Original Words (without autotuning)','Stretched Words (autotuning)');
title('Mapping Results');
xlabel('Notes'); ylabel('Average Frequency (Hz)');



%%  ALTERNATE MAPPING PROCEDURE (scrapped in favor of the current iteration)
% Will continue with development.

% % unsorted, just in order of the cut segments
% a = 1:length(averagevoice);
% b = 1:length(averagemusic);
% voice_unsort = [a',averagevoice']
% instr_unsort = [b',averagemusic']
% voice_vec = sortrows(voice_unsort,2)
% instr_vec = sortrows(instr_unsort,2)
% % now vectors are sorted in ascending frequency, segment numbers are
% % scrambled
% 
% %loop through and find adjacent frequencies that are close to each other.
% %When next freq is significantly higher than the previous, new voice word
% %assigned
% 
% % Quantization
% voiceRange = max(voice_vec(:,2))-min(voice_vec(:,2))
% 
% maxGroups = min(length(Quantize(voice_vec(:,2),0.01)), length(Quantize(instr_vec(:,2),0.01)));
% numGroupsWanted = 6; % inputted by user as per preference
% numGroups = min(numGroupsWanted, maxGroups); % some result from quantize
% voiceQuantInterval = findQuant(voice_vec(:,2),numGroups) % #intervals = #words
% QIndvoice = Quantize(voice_vec(:,2),voiceQuantInterval) 
% % instrQuantInterval = findQuant(instr_vec(:,2),length(QIndvoice)) % finds quantization interval that will ensure the input number of intervals
% QIndMusic = Quantize(instr_vec(:,2),instrQuantInterval)
% 
% % %%
% repeatedvoiceWords = [];
% for i = 1:length(QIndvoice) % i cycles through each quant interval of voice and repeats the words for each grouping of music
%     indexOfSound2Replace = QIndvoice{i}(1); % 1st word of each group will be used
%     length(QIndMusic{i})
%     repeatedvoiceWords = [repeatedvoiceWords,repmat(QIndvoice{i}(1),1,length(QIndMusic{i}))];
% end
% wordsInsteadofNotes = [instr_vec(:,1),repeatedvoiceWords'] % 1st col is note index, 2nd col is voice word index
% words2play = sortrows(wordsInsteadofNotes,1) 
% 
%%  ALTERNATE PLAYBACK/AUTOTUNING PROCEDURE (scrapped in favor of the current iteration)
% audioLenNeeded = 17; % number of seconds long the music piece is
% pauseTime = audioLenNeeded/length(words2play(:,2))
% numNotes=16; % choose how much many notes you want to hear
% VOL = 0.25; %The volume multiplier: attenuates the original music audio so we can hear voice's better
% for i = 1:length(words2play(1:numNotes,2))
%      wordNum = words2play(i,2);
% %     play(voicechopped{wordNum})
% %     play(musicchopped{i})
% %     pause(pauseTime);
%     
%     % changing pause time if word is longer
%     numSamples = length(voicechopped{1})    
%     timePerWord = numSamples/44100; % samples/(samples/s) = s
%     if (timePerWord>pauseTime)
%         pauseTime = timePerWord;
%     end
% 
% % Autotuning part
%     [x,y] = max(music_left_freqs{i});
%     fprintf('%2.0f\n',i);
%     musefreq = f_music{i}(y)    %CT music freq
%     [x,y] = max(voice_left_freqs{i+5});
%     voicefreq = f_voice{wordNum}(y) %CT voice freq
%     fff = 44100*voicefreq/musefreq; %new sampling freq to play at
%     while(fff<25000)    % change these limits until it sounds about right
%         fff = fff*2;    %alters the sampling frequency so it doesn't mess with the voice too much
%     end
%     while(fff>88000)
%         fff = fff/2;
%     end
%     fff                 %this line prints out fff.  semicolons were emitted earlier so i could visually see the numbers during testing
% % End of autotuning part
%
%     sound([left_voice_chopped{i+5},right_voice_chopped{i+5}],fff);
%     sound([left_music_chopped{i}*VOL,right_music_chopped{i}*VOL],44100);
%     pause(pauseTime); % pause for duration of 1 note
%     pauseTime = audioLenNeeded/length(words2play(:,2)); % reset pause time to default in case it was lengthened to do voice's word 
% end
% % Plotting Mapping results
% voiceyvals = voice_vec(words2play(:,2),2);
% figure;
% plot(words2play(:,1),voiceyvals,'r-', instr_unsort(:,1),instr_unsort(:,2),'b-','LineWidth',2);
% legend('voice','Music');
% title('Mapping Results: voice mapped words vs. music');
% xlabel('Words/notes'); ylabel('Frequency (Hz)');

