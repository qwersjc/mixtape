%Weighted Average Function
% used to find the weighted average of the frequencies in the DFT spectrum

function [weightedaverage]=WA(x,y)
weightedsum=0;
abba = length(x);
for i=1:length(x)
    weightedsum=x(i)*y(i)+weightedsum;
    if(x(i)*y(i) == 0)
        abba = abba - 1;
    end
end
weightedaverage=weightedsum/abba; 
%weightedaverage = max(y); considered using just the frequency with peak
%amplitude but desided to use weighted averag of all frequencies
end