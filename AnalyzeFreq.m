function [f, freqs]=AnalyzeFreq(sound)
% The goal of this function is to perform the fft of any inputted sound
% vector, which will return the frequency content of the spectrum. This
% code was constructed with the help of Matlab Documentation:
% http://www.mathworks.com/help/matlab/ref/fft.html
% input args:
%  sound - this can be any vector of a sound which was sampled at a freq
%          of 44100 Hz.
% output args:
%  f - the different frequencies of the inputted sound
%  amplitude - the amplitude of each frequency
    for i=1:length(sound)
        freq=abs(fft(sound{i}))/length(sound{i}); %performing fft
        freqs{i}=freq(1:floor((length(sound{i})/2+1)));
        freqs{i}(2:end-1)=2*freqs{i}(2:end-1); % creating freq vector
        f{i} = 44100*(0:(length(sound{i})/2))/length(sound{i});
    end
end
