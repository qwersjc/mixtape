function y = mixnmash(speak, music, autochk, shwplot)
% A distilled version of the main.
% speak - speaking signal
% music - music signal
% autochk - autotune?

	if(autochk)
		disp('mixing... (with autotune)');
	else
		disp('mixing...');
	end

    speak = .5*(speak(:,1) + speak(:,2));
    speak = speak(1:500000);
    music = .5*(music(:,1) + music(:,2));
    
    [s_seg, s_pos] = FAB(speak, .075, 420, 3000, shwplot, .2);
    [m_seg, m_pos] = FAB(music, .100, 200, 0000, shwplot, .2);
    %disp(length(s_seg));
    %disp(length(m_seg));
    
    [s_freqs,f_s]=AnalyzeFreq(s_seg);
    [m_freqs,f_m]=AnalyzeFreq(m_seg);
    
    for i=1:length(f_s)
        s_avg(i) = WA(f_s{i}, s_freqs{i});
    end
    for i=1:length(f_m)
        m_avg(i) = WA(f_m{i}, m_freqs{i});
    end
    
    numNotes = length(m_avg);
    
    s_stretch = max(m_avg)/max(s_avg)*s_avg;
    
    for i=1:length(m_avg)
        for j=1:length(s_avg)
            d(j) = abs(m_avg(i) - s_avg(j));
        end
        [~,notes_alloc(i)] = min(d);
    end

    speak_copy = music;

    for i = 1:length(notes_alloc)-3
        start = m_pos{i}(1); % 1 because it is starting index

        m = s_seg{notes_alloc(i)};
        n = numel(m);
        k1(i) = s_avg(notes_alloc(i)) / m_avg(i);
        xq = linspace(1, n, k1(i)*n);
        if autochk
            m2 = interp1(1:n, m, xq, 'nearest');
        else
            min_len = min([length(m), length(xq)]);
            m2 = m(1:min_len);
            m2 = m2';
        end
        finish = m_pos{i}(1) + length(m2) - 1;
        if finish > length(music)
            finish = length(music);
            m2 = m2(1:finish-start+1);        
        end
        speak_copy(start:finish) = .8 * music(start:finish) + m2';
    end

    sound(speak_copy, 44100);

    COMBINED_AUDIO = audioplayer(speak_copy, 44100);
    y = COMBINED_AUDIO; % doesn't actually seem to be stored in gui.
    %play(COMBINED_AUDIO);
    disp('done!');
    
    
end
