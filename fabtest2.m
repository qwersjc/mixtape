%% fabtest.m
% exploring the built-in matlab filters for preprocessing
clear, clc, close all;

bajwa = audioread('Bajwa40s.mp3');
bl = bajwa(1:(44100*5),1);

%bajwa = audioread('Guitar_Melody.wav');
%bl = bajwa(88000:end,1);

orig = audioplayer(bl, 44100);
%%
%[result, env, speaking, newaud] = FAB(bl, .075, 400,0,1,3);

n = 0:159;
x = cos(pi/8*n)+cos(pi/2*n)+sin(3*pi/4*n);

%Design an FIR equiripple bandpass filter to remove the lowest and highest discrete-time sinusoids.

d = fdesign.bandpass('Fst1,Fp1,Fp2,Fst2,Ast1,Ap,Ast2',1/4,3/8,5/8,6/8,60,1,60);
Hd = design(d,'equiripple');

%Apply the filter to the discrete-time signal.

y = filter(Hd,x);
freq = 0:(2*pi)/length(x):pi;
xdft = fft(x);
ydft = fft(y);
plot(freq,abs(xdft(1:length(x)/2+1)));
hold on;
plot(freq,abs(ydft(1:length(x)/2+1)),'r','linewidth',2);
legend('Original Signal','Bandpass Signal');

%%
%figure;
%plot(bajwa);
%figure;
%plot(result{1});


%%
for i = 1:length(result)
    arr{i} = audioplayer(result{i},44100);
end
pause(1);

%%
for i = 1:length(result)
    play(arr{i});
    pause(length(result{i})/44100);
    pause(.1);
end

