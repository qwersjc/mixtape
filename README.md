# README #
Digital Signal Processing Team Term Project: Quick Fix Audio Mix

This is a DSP final project dedicated to bringing you new music in the form of the humble mixtape concept

5/12/16

### What does this repository contain? ###

This repository contains several things, including several functions used in our project, a main(for testing), a gui that can be run in matlab, and audio samples.

### How do I get set up? ###

* Download the code.
* Open up gui.m and run it.
* Requires MATLAB.

### What audio and music pieces are suggested for testing? ###

Combo1 - Audio: Obama_State_Union.mp3, Music: Sweet_Child_Of_Mine_Sample.wav

Combo2 - Audio: EconLec.mp3, Music: Pirates of the Caribbean - He's A Pirate.mp3

Combo3 - Audio: Prof40s.mp3, Music: Guitar_Melody.wav

### Who do I talk to? ###

Team: Jack Aquino, Matthew Chan, Abbas Furniturewalla, Jaimie Swartz